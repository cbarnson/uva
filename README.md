README
======

### Author
Cody Barnson


# Problem Topics
This section lists various competitive programming problem concepts and related sub-concepts, along with problems that I have personally solved for reference.

## Graph/Shortest Path

#### Dijkstra
1. /uva/11367.cc
    * A variation of dijkstra single-source shortest path algorithm that uses a priority queue to track state information associated with the problem.  In this problem we want the cheapest cost possible to reach our destination.  Each node represents a city, with a gas station, and has a price/L of fuel associated with it.  Edges represent the integer quantity of fuel required to travel across the edge (i.e. road).  

## String manipulation

#### Suffix array
1. /uva/719.cc
    * Given a string, which is circularly connected to itself, we are asked to find the worst possible (lexicographically) disconnect (i.e. if the circular string broke at some index from 0 to length-1).  We use Howard's implementation of a suffix array from the code library and adapt it to this problem.  Key note is appending the same string to itself and using the suffix array functions on that.
2. /uva/11512.cc


## Other

#### Ordinal and cardinal number conversion
1. /uva/rocky2010/solved/4890.cc

#### Base conversion
1. /uva/10047.ccs