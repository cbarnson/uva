// Cody Barnson
// 10511 - Councilling

#include <bits/stdc++.h>

using namespace std;

struct Edge;
typedef list<Edge>::iterator EdgeIter;

struct Edge {
   int to;
   int cap;
   int flow;
   bool is_real;
   EdgeIter partner;
  
   Edge(int t, int c, bool real = true)
      : to(t), cap(c), flow(0), is_real(real)
   {};

   int residual() const
   {
      return cap - flow;
   }
};

struct Graph {
   list<Edge> *nbr;
   int num_nodes;
   Graph(int n)
      : num_nodes(n)
   {
      nbr = new list<Edge>[num_nodes];
   }

   ~Graph()
   {
      delete[] nbr;
   }

   // note: this routine adds an edge to the graph with the specified capacity,
   // as well as a residual edge.  There is no check on duplicate edge, so it
   // is possible to add multiple edges (and residual edges) between two
   // vertices
   void add_edge(int u, int v, int cap)
   {
      nbr[u].push_front(Edge(v, cap));
      nbr[v].push_front(Edge(u, 0, false));
      nbr[v].begin()->partner = nbr[u].begin();
      nbr[u].begin()->partner = nbr[v].begin();
   }
};

void push_path(Graph &G, int s, int t, const vector<EdgeIter> &path, int flow)
{
   for (int i = 0; s != t; i++) {
      if (path[i]->is_real) {
	 path[i]->flow += flow;
	 path[i]->partner->cap += flow;
      } else {
	 path[i]->cap -= flow;
	 path[i]->partner->flow -= flow;
      }
      s = path[i]->to;
   }
}

// the path is stored in a peculiar way for efficiency: path[i] is the
// i-th edge taken in the path.
int augmenting_path(const Graph &G, int s, int t, vector<EdgeIter> &path,
		    vector<bool> &visited, int step = 0)
{
   if (s == t) {
      return -1;
   }
   for (EdgeIter it = G.nbr[s].begin(); it != G.nbr[s].end(); ++it) {
      int v = it->to;
      if (it->residual() > 0 && !visited[v]) {
	 path[step] = it;
	 visited[v] = true;
	 int flow = augmenting_path(G, v, t, path, visited, step+1);
	 if (flow == -1) {
	    return it->residual();
	 } else if (flow > 0) {
	    return min(flow, it->residual());
	 }
      }
   }
   return 0;
}

// note that the graph is modified
int network_flow(Graph &G, int s, int t)
{
   vector<bool> visited(G.num_nodes);
   vector<EdgeIter> path(G.num_nodes);
   int flow = 0, f;

   do {
      fill(visited.begin(), visited.end(), false);
      if ((f = augmenting_path(G, s, t, path, visited)) > 0) {
	 push_path(G, s, t, path, f);
	 flow += f;
      }
   } while (f > 0);
  
   return flow;
}

void print(int i, int j, int w) {
   cout << "Edge: " << i << " to " << j << ", weight: " << w << endl;
}

int main() {

   // representation of node for source and sink
   int source = 3000, sink = 3001;
   const int poff = 1000, coff = 2000;
   
   // read number of cases
   int n;
   cin >> n >> ws;

   while (n--) {


      map<string, int> party;
      map<string, int> club;
      map<int, string> iparty;
      map<int, string> iclub;
      map<int, string> iperson;
      
      int ip = 0, ic = 0;
      
      map<string, vector<int> > input;
      string line;
      while (getline(cin, line) && line != "") {

	 string resident, str;
	 vector<int> info;
	 istringstream iss(line);
	 iss >> resident;
	 // party
	 iss >> str;
	 int id;
	 auto it = party.find(str);
	 if (it == party.end()) {
	    id = poff + ip++;
	    party.insert(make_pair(str, id));
	    iparty.insert(make_pair(id, str));
	 } else
	    id = party[str];
	 info.push_back(id);
	 
	 while (iss >> str) {
	    // cout << "'" << str << "'" << endl;
	    auto it = club.find(str);
	    if (it == club.end()) {
	       id = coff + ic++;
	       club.insert(make_pair(str, id));
	       iclub.insert(make_pair(id, str));
	    } else
	       id = club[str];
	    info.push_back(id);
	 }
	 		 
	 input[resident] = info;	 
      }

      Graph g(3002);
      int sz = club.size();
      // int sz = party.size();
      if (sz < 3) {
	 cout << "Impossible." << endl;
	 if (n) cout << endl;
	 continue;
      }
      int sflow = (sz - 1) / 2;
      // cout << "sflow: " << sflow << endl;

      bool seenp[3000];
      fill(seenp, seenp+3000, false);
      
      // add edges
      int i = 0;
      for (auto &it : input) {

	 iperson.insert(make_pair(i, it.first));
	 
	 // connect source to party
	 int pt = it.second[0];
	 if (!seenp[pt]) {
	    g.add_edge(source, pt, sflow);
	    seenp[pt] = true;
	 }

	 // connect party to person
	 g.add_edge(pt, i, 1);

	 // connect person to club
	 for (int j = 1; j < it.second.size(); j++) {
	    g.add_edge(i, it.second[j], 1);    // person to club
	 }
	 
	 // next person
	 i++;
      }

      // clubs to sink
      for (auto &it : club) {
	 g.add_edge(it.second, sink, 1);
      }
      

      int flow = network_flow(g, source, sink);
      int C = club.size();
      if (flow < C) {
	 cout << "Impossible." << endl;
	 if (n) cout << endl;
	 continue;
      }

      int N = input.size();
      for (int i = 0; i < N; i++) {
	 for (auto &j : g.nbr[i]) {
	    if (j.flow > 0 && j.is_real) {
	       // cout << "i, j: " << i << " " << j.to << endl;
	       cout << iperson[i] << " " << iclub[j.to] << endl;
	    }
	 }
      }

      if (n) cout << endl;
      
      // debug -- print info for each resident
      // for (auto it = input.begin(); it != input.end(); ++it) {
      // 	 cout << "resident: " << it->first << ", ";
      // 	 for (auto j = it->second.begin(); j != it->second.end(); ++j) {
      // 	    cout << *j << " ";
      // 	 }
      // 	 cout << endl;
      // }
      // cout << endl;
      









      

      // map<string, int> si_person;
      // map<string, int> si_party;
      // map<string, int> si_club;

      // map<int, string> is_person;
      // map<int, string> is_party;
      // map<int, string> is_club;

      // int i_per = 0, i_par = 0, i_clu = 0;

      // int off_par = 2;
      // int off_per = 1000 + 2;
      // int off_clu = 2000 + 2;

      // vector<int> v[1000]; // up to 1000 lines

      // int l = 0; // line count
      // string line;
      // while (getline(cin, line) && line != "") {
      // 	 istringstream iss(line);
      // 	 string a, b, c;
      // 	 iss >> a >> b;


      // 	 // if party not seen, add
      // 	 if (!si_party[b]) {
      // 	    si_party[b] = i_par + off_par;
      // 	    is_party[si_party[b]] = b;
      // 	    i_par++;
      // 	 }
      // 	 v[l].push_back(si_party[b]);

      //    // if person not seen, add
      // 	 if (!si_person[a]) {
      // 	    si_person[a] = i_per + off_per;
      // 	    is_person[si_person[a]] = a;
      // 	    i_per++;
      // 	 }
      // 	 v[l].push_back(si_person[a]);
	 
      // 	 while (iss >> c) {
      // 	    // if club not seen, add
      // 	    if (!si_club[c]) {
      // 	       si_club[c] = i_clu + off_clu;
      // 	       is_club[si_club[c]] = c;
      // 	       i_clu++;
      // 	    }
      // 	    v[l].push_back(si_club[c]);        
      // 	 }

      // 	 // increment line index
      // 	 l++;
      // }

      // // debug, print input
      // // for (int i = 0; i < l; i++) {
      // // 	 cout << is_person[v[i][1]] << "," << v[i][1] << " " << is_party[v[i][0]] << "," << v[i][0] << " ";      
      // //   for (int j = 2; j < v[i].size(); j++) {
      // // 	   cout << is_club[v[i][j]] << "," << v[i][j];
      // // 	   if (j != v[i].size() - 1) cout << " ";
      // //   }
      // //   cout << endl;
      // // }
      // // cout << endl;


      // int num_parties = si_party.size();
      // if (num_parties < 3) { // impossible
      // 	 cout << "Impossible." << endl;
      // 	 if (n) cout << endl;
      // 	 continue;
      // }

      // // cout << num_parties << endl;
      
      // int src_flow = (num_parties - 1) / 2;

      // // cout << "src flow " << src_flow << endl;

      // int src = 0;
      // int sink = 1;
      // Graph g(3002);      

      // // bool seen[3002];
      // // fill(seen, seen+3002, false);


      // // add edges to each party
      // for (auto it = is_party.begin(); it != is_party.end(); ++it) {
      // 	 g.add_edge(src, it->first, src_flow);
      // 	 // cout << "src to party: " << src << " " << it->first << endl;
      // }

      // // add edges to sink for each club
      // for (auto it = is_club.begin(); it != is_club.end(); ++it) {
      // 	 g.add_edge(it->first, sink, 1);
      // 	 // cout << "club to sink: " << it->first << " " << sink << endl;
      // }

      // // add edges for each party to person
      // for (int i = 0; i < l; i++) {
      // 	 // v[i][1] is person, v[i][0] is party
      // 	 g.add_edge(v[i][0], v[i][1], 1);
      // 	 // cout << "edge: " << v[i][0] << " to " << v[i][1] << endl;
      // 	 // cout << "party to person: " << v[i][0] << " " << v[i][1] << endl;
      // }

      // // add edges from person to clubs
      // for (int i = 0; i < l; i++) {
      // 	 for (int j = 2; j < v[i].size(); j++) {
      // 	    g.add_edge(v[i][1], v[i][j], 1);
      // 	    // cout << "person to club: " << v[i][1] << " " << v[i][j] << endl;
      // 	 }
      // }

      // int flow = network_flow(g, src, sink);
      // // cout << "flow is " << flow << ", num clubs: " << si_club.size() << endl;
      // if (flow != si_club.size()) {
      // 	 cout << "Impossible." << endl;
      // } else {

      // 	 vector< pair<string, string> > soln;	 
      // 	 for (auto &i : g.nbr[0]) {
      // 	    // cout << "src to party: " << 0 << " " << i.to << endl;
      // 	    if (i.flow > 0 && i.is_real) {
      // 	       for (auto &j : g.nbr[i.to]) {
      // 		  // bool used = false;
      // 		  if (j.flow > 0 && j.is_real) {
      // 		     for (auto &k : g.nbr[j.to]) {
      // 			if (k.flow > 0 && k.is_real) {
      // 			   for (auto &m : g.nbr[k.to]) {
      // 			      if (m.flow > 0 && m.is_real) {
      // 				 soln.push_back(make_pair(is_person[j.to], is_club[k.to]));
      // 				 // used = true;
      // 				 // break;
      // 			      }
      // 			   }
      // 			   // if (used) break;
      // 			}
      // 		     }		     
      // 		  }
      // 	       }
      // 	    }
      // 	 }
      // 	 sort(soln.begin(), soln.end());
      // 	 for (auto &it : soln) {
      // 	    cout << it.first << " " << it.second << endl;
      // 	 }
      // }


      // if (n) cout << endl;
      
    
   }

  
   return 0;
}


