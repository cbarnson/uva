#include <bits/stdc++.h>
using namespace std;

typedef long long ll;

int main() {

   // int x = (7 << 3);
   // int y = (7 << 1);
   // cout << x << " + " << y << endl;
   // cout << bitset<8>(x) << " + " << bitset<8>(y) << endl;
   // return 0;

   
   ll n;
   cout << "Enter a number in the range [1, 2 * pow(10, 100)]: ";
   cin >> n;

   for (ll i = 1; i <= n; i++) {
      ll ans = pow(i, i);
      cout << "pow(" << i << ", " << i << ") : \t" << pow(i, i) << endl;
   }
   cout << endl;

   for (ll i = 1; i <= n; i++) {
      cout << "sum of first " << i << " terms: ";
      ll sum = 0;
      for (ll j = 1; j <= i; j++) {
	 sum += pow(j, j);
      }
      cout << to_string(sum).back() << "\t" << sum << endl;
   }
   cout << endl;
   
   return 0;
}
