#include <bits/stdc++.h>
#include <cassert>
using namespace std;

// # of choices for each property
// rowIndex, colIndex, direction, colorWheel
int p[4] = { 25, 25, 4, 5 };

// map x onto array
void toa(int a[4], int x) {
   for (int i = 0; i < 4; ++i) {
      int divisor = 1;
      for (int j = 0; j < i; ++j) 
	 divisor *= p[j];      
      int quotient = x / divisor;
      a[i] = quotient % p[i];
   }
}

// given array representing state, map the elements onto a
// single integer and return it
int toi(int a[4]) {
   int x = a[0];
   for (int i = 1; i < 4; ++i) {
      int tt = a[i];
      for (int j = i-1; j >= 0; --j) 
	 tt *= p[j];      
      x += tt;
   }
   return x;
}

int main() {

   int x;
   while (cin >> x) {
      if (x < 0 || x >= 12500) {
	 cout << "Please enter a number in the range (0, 12500]." << endl;
	 continue;
      }
      int y[4];
      toa(y, x);
      for (int i = 0; i < 4; i++) {
	 cout << y[i] << " \n"[i < 4];
      }      
   }
   

   return 0;
}
