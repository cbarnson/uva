// Problem    : UVa 230 - Borrowers
// Time limit : 3.000 seconds
// Difficulty : 3
// Author     : Cody Barnson
// Date       : Jan. 13, 2018
#include <bits/stdc++.h>
using namespace std;

int main() {
   
   // std::set
   set<int> a; 

   int arr[] = { 1, 2, 3, 4 };
   set<int> b (arr, arr+4);


   int x = 3, y = 4, z = 5;

   // check if element is in the set
   // set::count, O(log(size))
   if (b.count(x) != 0) {
      cout << "x is in b" << endl;
   } else {
      cout << "x is not in b" << endl;
   }

   // get iterator to element in set
   // set::find, O(log(size))
   auto it = b.find(y);
   if (it != b.end()) {
      cout << "y is in b" << endl;
      // erase set element that iterator is pointing to
      // set::erase, O(1)
      b.erase(it);
   } else {
      cout << "y is not in b" << endl;
   }
   

   return 0;
}
