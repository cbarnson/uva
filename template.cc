// Problem:
// Status :
#include <bits/stdc++.h>
using namespace std;

#define mp make_pair
#define pb push_back
#define INF (int)1e9
#define EPS 1e-9
#define bitcount __builtin_popcount
#define GCD __gcd

typedef pair<int, int> pii;
typedef vector<pii> vii;

int multBy2(int num) { return num << 1; }
int divBy2(int num) { return num >> 1; }
int numberOfDigits(int num) { return floor(log10(num)) + 1; }
bool isOdd(int num) { return (num & 1); }
bool isPowerOf2(int x) { return x && (!(x & (x - 1))); }; // for positive integers
void printBinaryRep(int x) { cout << bitset<32>(x).to_string() << endl; }

const double PI = acos(-1.0);
const double E = exp(1.0);

int main() {

   
   return 0;
}
